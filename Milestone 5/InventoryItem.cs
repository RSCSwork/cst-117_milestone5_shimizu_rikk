﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

//This is my work, Rikk Shimizu


namespace MilestoneProject
{
    class InventoryItem
    {
        //these variables are "public" but only get is made public.  the set is still private as specified by the name.
        //This idea was found on stack overflow.
        public string ItemName { get; private set; }
        public int ItemID { get; private set; }
        public string ItemDescription  { get; private set; }
        public int ItemQuantity { get; private set; } = 0;
        public string ItemManufacturer { get; private set; }
        public double PricePerItem { get; private set; }
  
        /*
         * A Constructor for the Inventory item class/objects
         * @params string itemName, int itemID, string itemDescription, int itemQuantity, string itemManufacturer, double pricePerItem
         * 
         * This constructor will also serve as part of the unit testing, to prove an item can be created/added to the table.
         * Since this week the milestone is meant to start getting functionality and prove that functionality, it made sense to do a basic setting
         * of an InventoryItem and show it in the XML on the GUI.
         */
        public InventoryItem(string itemName, int itemID, string itemDescription, int itemQuantity, string itemManufacturer, double pricePerItem)
        {
            this.ItemName = itemName;
            this.ItemID = itemID;
            this.ItemDescription = itemDescription;
            this.ItemQuantity = itemQuantity;
            this.ItemManufacturer = itemManufacturer;
            this.PricePerItem = pricePerItem;
        }//ends constructor

        /*
         * This will help to add to the current quantity
         * such as when inventory is delivered. 
         */
        public void addQuantity(int addThisMany)
        {
            this.ItemQuantity += addThisMany;
        }//ends add quantity

        /*
         * this will help to remove from the current quantity
         * such as during a purchase
         */
        public void removedQuantity(int subtractThisMany)
        {
            this.ItemQuantity -= subtractThisMany;
        }//ends removed quantity

    }//ends Inventory Item class
}//ends name space
