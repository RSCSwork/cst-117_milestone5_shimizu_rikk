﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MilestoneProject
{
    public partial class addedWindow : Form
    {
        public addedWindow()
        {
            InitializeComponent();
            addedLbl.Text = "Your item has been added to the inventory"; 

        }//ends added window

        private void CloseBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }//ends close btn
    }
}
